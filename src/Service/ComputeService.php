<?php

namespace App\Service;


class ComputeService{

    //Function to make a sum
    public function compute_sum(int $num1, int $num2){
        return $num1 + $num2;
    }

    //Function to make a substraction
    public function compute_substraction(int $num1, int $num2){
        if($num2>$num1){
            return "Substraction not possible";
        }else {
            return $num1 - $num2;
        }   
    }

    //Function to make a multication
    public function compute_multi(int $num1, int $num2): int{
        return $num1 * $num2;
    }

    //Function to make a division
    public function compute_division(int $num1, int $num2){
        if($num1 == 0){
            return "Division impossbile by zero";
        }else{
            return $num1 / $num2;
        }
    }
}
