install:
	composer install

start:
	php bin/console server:run

test_unit:
	bin/phpunit tests/Unit

test_integration:
	bin/phpunit tests/Integration

test: test_unit test_integration

start_docker:
	docker-compose up

stop_docker:
	docker-compose down