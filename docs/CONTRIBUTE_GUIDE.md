<p align="center">
  <a href="https://symfony.com" target="_blank">
    <img src="https://symfony.com/logos/symfony_black_02.svg">
 </a>

  <h3 align="center">Projet Symfony - Mixer</h3>

  <p align="center">
    Project make with symfony, view liste of mixer
    <br />
    <a href="https://Gitlab.com/othneildrew/Best-README-Template/issues">Readme</a>
    -
    <a href="https://Gitlab.com/othneildrew/Best-README-Template/issues">Code of Conduct</a>
  </p>
</p>
# Contributing to Open Source Guides

Thanks for checking out the Open Source Guides! We're excited to hear and learn from you. Your experiences will benefit others who read and use these guides.

We've put together the following guidelines to help you figure out where you can best be helpful.

## Types of contributions we're looking for
There are many ways you can directly contribute to the guides (in descending order of need):

* Fix editorial inconsistencies or inaccuracies
* Add stories, examples, or anecdotes that help illustrate a point
* Revise language to be more approachable and friendly

## Ground rules & expectations

Before we get started, here are a few things we expect from you (and that you should expect from others):

* Be kind and thoughtful in your conversations around this project. We all come from different backgrounds and projects, which means we likely have different perspectives on "how open source is done." Try to listen to others rather than convince them that your way is correct.
* Open Source Guides are released with a [Contributor Code of Conduct](./CODE_OF_CONDUCT.md). By participating in this project, you agree to abide by its terms.
* If you open a pull request, please ensure that your contribution passes all tests. If there are test failures, you will need to address them before we can merge your contribution.
* When adding content, please consider if it is widely valuable. Please don't add references or links to things you or your employer have created as others will do so if they appreciate it.

## How to contribute

If you'd like to contribute, start by searching through the [issues](https://Gitlab.com/Gitlab/opensource.guide/issues) and [pull requests](https://Gitlab.com/Gitlab/opensource.guide/pulls) to see whether someone else has raised a similar idea or question.

If you don't see your idea listed, and you think it fits into the goals of this guide, do one of the following:
* **If your contribution is minor,** such as a typo fix, open a pull request.
* **If your contribution is major,** such as a new guide, start by opening an issue first. That way, other people can weigh in on the discussion before you do any work.


## Workflow

The development workflow is based on the [Gitlab flow](https://guides.Gitlab.com/introduction/flow/) :

* Every code change (new feature, bug fix, etc.) should result from a ticket (*issue*)
* chaque changement de code devrait passer une *review* de code via une Pull Request

The right way to create a Merge Request is to :

* Locally create a new branch corresponding to the ticket / issue you want to process via```git checkout develop && git pull && git checkout -b fix/13```
* Modify the code and accumulate the commits in this new branch. Commits should mention the corresponding ticket (use the keywords *fix* or *see*)
* Push code to GitLab ```git push origin fix/13```
* Create merge Request

## Contact :email:
contact@exemple.fr - email@example.com