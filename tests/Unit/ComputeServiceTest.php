<?php

namespace App\Tests\Unit;

use App\Service\ComputeService;
use PHPUnit\Framework\TestCase;

class ComputeServiceTest extends TestCase
{
    public function testcompute_sum()
    {
        $ComputeService = new ComputeService();
        $result = $ComputeService->compute_sum(1, 1);

        // assert that your calculator added the numbers correctly!
        $this->assertEquals(2, $result);
    }

    public function testcompute_substraction()
    {
        $ComputeService = new ComputeService();
        $result = $ComputeService->compute_substraction(3, 2);

        // assert that your calculator added the numbers correctly!
        $this->assertEquals(1, $result);
    }

    public function testcompute_multi()
    {
        $ComputeService = new ComputeService();
        $result = $ComputeService->compute_multi(3, 3);

        // assert that your calculator added the numbers correctly!
        $this->assertEquals(9, $result);
    }

    public function testcompute_division()
    {
        $ComputeService = new ComputeService();
        $result = $ComputeService->compute_division(15, 5);

        // assert that your calculator added the numbers correctly!
        $this->assertEquals(3, $result);
    }
}

?>