<?php

namespace App\Service\MixerService;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use GuzzleHttp\Client;

class MixerServiceTest extends WebTestCase
{
    public function testgetData()
    {
        $url = 'https://mixer.com/api/v1/channels?order=viewersCurrent:DESC&limit=20&where=languageId:eq:fr';
       
        $client = new \GuzzleHttp\Client();

        $response=$client->request('GET', $url);

        $this->assertEquals(200, $response->getStatusCode()); //testing the return code is 200
        $body = json_decode($response->getBody(), true);

        $this->assertGreaterThan(0, count($body[0]));
        $this->assertArrayHasKey('featured',  $body[0]);//Check if the value "featured" is returned
        $this->assertArrayHasKey('id',  $body[0]);//Check if the value "id" is returned
        $this->assertArrayHasKey('token',  $body[0]);//Check if the value "token" is returned
        
        $this->assertIsBool($body[0]['online']);  //Check if the value "online" is boolean 
        $this->assertIsString($body[0]['description']); //Check if the value "description" is string
        $this->assertIsInt($body[0]['viewersTotal']); //Check if the value "viewersTotal" is int
    }
}
 ?>
