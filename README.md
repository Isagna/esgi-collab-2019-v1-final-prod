<!-- PROJECT LOGO -->
<p align="center">
  <a href="https://symfony.com" target="_blank">
    <img src="https://symfony.com/logos/symfony_black_02.svg">
 </a>

  <h3 align="center">Projet Symfony - Mixer
  <br/>
  <br/>

  ![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/Isagna/esgi-collab-2019-v1-final-prod?style=for-the-badge)  ![Cocoapods](https://img.shields.io/cocoapods/l/AFNetworking?style=for-the-badge)

  </h3>

  <p align="center">
    Project make with symfony, view liste of mixer
    <br />
    <a href="https://github.com/othneildrew/Best-README-Template/issues">Code of Conduct</a>
    -
    <a href="https://github.com/othneildrew/Best-README-Template">View Demo</a>
    -
    <a href="https://github.com/othneildrew/Best-README-Template/issues">Contributing Guide</a>
  </p>
</p>

## Table of Contents :mag:

* [About Project](#about-the-project)
* [Built With](#built-with)
* [Prerequisites](#about-the-project)
* [Usage](#about-the-project)
* [Running Test](#running-the-tests)
* [Code of Conduct](#code-of-conduct)
* [Contributing Guide](#contributing-guide)

    <br />

## About The Project

Mixer is a <b>Seattle-based</b> video game streaming service owned by Microsoft. The service was officially launched on January 5, 2016 under the name of Beam, but was renamed Mixer in May 2017.

The service focuses on interactivity where viewers can, among other things, perform actions that will have an influence on the live. In addition, live broadcasts benefit from low latency.

### Built With :dizzy:

* [Symfony](https://symfony.com)
* [Docker](https://docker.com)
* [Heroku](https://heroku.com)
* [Gitlab CI](https://gitlab.com)

    <br />

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
1. Docker
2. Php
3. Composer
4. Make
5. Git

## Usage

  <b>Local</b>

1. Clone the repo
```sh
git clone https://gitlab.com/Isagna/esgi-collab-2019-v1-final-prod.git
```

2. Install
```bash
$ make install
```
3 Start project localy
```bash
$ make start
```


3 bis Start project with Docker

Start project with docker
```bash
$ make start_docker
```

Stop project docker
```bash
$ make stop_docker
```

  <br/>

  <b>Heroku</b>

1. View the Heroku project
```sh
Projet https://esgi-collab-2019.herokuapp.com/
```

2. Clone heroku project
```bash
$ heroku git:clone -a esgi-collab-2019
$ cd esgi-collab-2019
```

  <br />

### Running the tests
1. Test unitaire
```sh
$ make test:unit
```

2. Test d'integration
```sh
$ make test:integration
```

3. Test All
```sh
$ make test
```

<br/>

### Code of Conduct

Please read [the full text](https://code.fb.com/codeofconduct) so that you can understand what actions will and will not be tolerated.

### Contributing Guide

Read our [contributing guide](https://reactjs.org/contributing/how-to-contribute.html) to learn about our development process.

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m '<Subject>(scope): AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Create Merge Request

  <br />

## Acknowledgements
* [Issa SAGNA](https://www.webpagefx.com/tools/emoji-cheat-sheet)
* [Yanis ELMALEH](https://shields.io)
* [Levy ZIRE](https://choosealicense.com)
* [Ayoub IBNOUCHEIKH](https://pages.github.com)

  <br />

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Contact :email:
contact@exemple.fr - email@example.com